﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DbContexts;
using DAL.Interfaces;
using DAL.Repositories;

namespace DAL.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ShopSQLContext _dbContext = new ShopSQLContext();
        private OrderRepository _orderRepository;
        private ProductRepository _productRepository;
        private OrderItemRepository _orderItemRepository;
        private bool disposed = false;

        public UnitOfWork()
        {
        }

        public OrderRepository Orders
        {
            get
            {
                if (this._orderRepository == null)
                {
                    this._orderRepository = new OrderRepository(_dbContext);
                }
                return _orderRepository;
            }
        }

        public OrderItemRepository OrderItems
        {
            get
            {

                if (this._orderItemRepository == null)
                {
                    this._orderItemRepository = new OrderItemRepository(_dbContext);
                }
                return _orderItemRepository;
            }
        }

        public ProductRepository Products
        {
            get
            {

                if (this._productRepository == null)
                {
                    this._productRepository = new ProductRepository(_dbContext);
                }
                return _productRepository;
            }
        }


        public void Save()
        {
            _dbContext.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
