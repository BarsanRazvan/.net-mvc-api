﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using DAL.DbContexts;

namespace DAL.Repositories
{
    public class OrderRepository : GenericRepository<Order>
    {
        public OrderRepository(ShopSQLContext dbContext) : base(dbContext)
        {

        }
    }
}
