﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using DAL.DbContexts;

namespace DAL.Repositories
{
    public class ProductRepository : GenericRepository<Product>
    {
        public ProductRepository(ShopSQLContext dbContext) : base(dbContext)
        {

        }
    }

}
