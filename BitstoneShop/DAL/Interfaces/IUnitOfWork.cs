﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        OrderRepository Orders { get; }
        OrderItemRepository OrderItems { get ; }
        ProductRepository Products { get; }
        void Save();
    }
}
