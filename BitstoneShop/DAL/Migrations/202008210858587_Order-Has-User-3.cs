﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderHasUser3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Order", "UserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "UserId", c => c.Single(nullable: false));
        }
    }
}
