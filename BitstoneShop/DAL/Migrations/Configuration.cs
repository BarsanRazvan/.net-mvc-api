﻿using Common.Entities;
using DAL.DbContexts;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Migrations
{
    public class Configuration : DbMigrationsConfiguration<ShopSQLContext>
    {
        protected override void Seed(ShopSQLContext context)
        {
            var products = new List<Product>
            {
                new Product()
                {
                    Name = "Milk",
                    Description = "This is goood milk",
                    Category = ProductCategory.FOOD,
                    Price = 2.00f
                },
                new Product()
                {
                    Name = "Honey",
                    Description = "This is ok honey",
                    Category = ProductCategory.FOOD,
                    Price = 12.00f
                },
                new Product()
                {
                    Name = "Mug",
                    Description = "This is a bitstone mug",
                    Category = ProductCategory.MISC,
                    Price = 4.00f
                },
                new Product()
                {
                    Name = "Apple Xs",
                    Description = "Should I buy this?",
                    Category = ProductCategory.ELECTRONICS,
                    Price = 122.00f
                },
                new Product()
                {
                    Name = "Pants",
                    Description = "Looking fancy",
                    Category = ProductCategory.CLOTHES,
                    Price = 20.00f
                },
                new Product()
                {
                    Name = "Dogs",
                    Description = "Looking fancy",
                    Category = ProductCategory.CLOTHES,
                    Price = 20.00f
                }
            };
            products.ForEach(prod => context.Products.AddOrUpdate(prod));
            context.SaveChanges();

        }
    }
}
