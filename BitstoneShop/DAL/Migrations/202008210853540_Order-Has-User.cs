﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderHasUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "UserId", c => c.Single(nullable: false));
            AddColumn("dbo.Order", "User_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Order", "User_Id");
            AddForeignKey("dbo.Order", "User_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Order", "CustomerName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order", "CustomerName", c => c.String());
            DropForeignKey("dbo.Order", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Order", new[] { "User_Id" });
            DropColumn("dbo.Order", "User_Id");
            DropColumn("dbo.Order", "UserId");
        }
    }
}
