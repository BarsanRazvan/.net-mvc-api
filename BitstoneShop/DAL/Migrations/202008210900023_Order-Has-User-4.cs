﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderHasUser4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Order", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order", "UserId", c => c.Int(nullable: false));
        }
    }
}
