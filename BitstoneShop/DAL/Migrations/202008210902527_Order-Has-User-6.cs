﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderHasUser6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Order", "UserId");
            AddForeignKey("dbo.Order", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Order", new[] { "UserId" });
            DropColumn("dbo.Order", "UserId");
        }
    }
}
