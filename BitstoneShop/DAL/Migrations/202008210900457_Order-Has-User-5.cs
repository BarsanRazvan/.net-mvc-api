﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderHasUser5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Order", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Order", new[] { "User_Id" });
            DropColumn("dbo.Order", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order", "User_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Order", "User_Id");
            AddForeignKey("dbo.Order", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
