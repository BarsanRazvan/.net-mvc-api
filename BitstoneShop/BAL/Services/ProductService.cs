﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BAL.Interfaces;
using Common.Entities;
using Common.Utilities;
using Common.ViewModels;
using DAL.Interfaces;

namespace BAL.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public bool AddNewProduct(ProductModel product)
        {
            try{
                var mappedProduct = _mapper.Map<ProductModel,Product>(product);
                this._unitOfWork.Products.Insert(mappedProduct);
                this._unitOfWork.Save();
                return true;
            }catch
            {
                return false;
            }
        }

        public IEnumerable<ProductModel> GetAllProducts()
        {
            return _mapper.Map<IEnumerable<ProductModel>>(_unitOfWork.Products.Get(null, null, ""));
        }

        public PagedList<ProductModel> GetPagedProducts(int pageNo, int pageSize)
        {
            var products = _unitOfWork.Products.GetPage(pageNo, pageSize,
                                                                    query => query.OrderBy(o => o.ID));

            var mappedProducts = new PagedList<ProductModel>(new List<ProductModel>(), products.TotalCount, products.CurrentPage, products.PageSize);

            products.ForEach(product =>
            {
                mappedProducts.Add(_mapper.Map<Product, ProductModel>(product));
            });

            products.Select(x => _mapper.Map<Product, ProductModel>(x)).ToList();
            return mappedProducts;
        }

        public ProductModel GetProductById(int id)
        {
            return _mapper.Map<Product,ProductModel>(_unitOfWork.Products.GetByID(id));
        }

        public IEnumerable<ProductModel> GetProductsByIds(IEnumerable<int> productIds)
        {
            var mappedProducts = new List<ProductModel>();

            productIds.ToList().ForEach(productId =>
            {
                Product product = _unitOfWork.Products.GetByID(productId);
                if (product != null)
                {
                    mappedProducts.Add(_mapper.Map<Product, ProductModel>(product));
                }
            });

            return mappedProducts;
        }

        public CartModel GetItemsFromCart(CartModel cart)
        {
            var itemIds = cart.Items.Select(item => item.ID);
            var products = _unitOfWork.Products.Get(product => itemIds.Contains(product.ID)).ToList();

            var mappedProducts = _mapper.Map<List<ProductModel>>(products);

            cart.UpdateProductDetails(mappedProducts);

            return cart;
        }

        public bool EditProduct(ProductModel product)
        {
            if (_unitOfWork.Products.GetByID(product.ID) == null)
            {
                return false;
            }

            var mappedProduct = _mapper.Map<ProductModel, Product>(product);
            _unitOfWork.Products.Update(mappedProduct);
            _unitOfWork.Save();
            return true;
        }

        public bool DeleteProduct(int id)
        {
            var objectExists = _unitOfWork.Products.GetByID(id) != null;
            if (objectExists)
            {
                _unitOfWork.Products.Delete(id);
                _unitOfWork.Save();
                return true;
            }

            return false;
        }
    }
}
