﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BAL.Interfaces;
using Common.Entities;
using Common.Utilities;
using Common.ViewModels;
using DAL.Interfaces;

namespace BAL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public OrderModel AddNewOrder(CartModel cart, string userId)
        {
            var newOrder= new Order()
            {
                UserId = userId,
                OrderDate = DateTime.Now,
                Total = 0
            };

            var mappedOrder = _mapper.Map<Order, OrderModel>(newOrder);

            _unitOfWork.Orders.Insert(newOrder);

            cart.Items.ForEach(cartItem =>
            {
                var item = new OrderItem();
                var product = _unitOfWork.Products.GetByID(cartItem.ID);
                item.Order = newOrder;
                item.Product = product;
                item.Quantity = cartItem.Quantity;
                item.CurrentPrice = product.Price;

                _unitOfWork.OrderItems.Insert(item);

                newOrder.Total += product.Price * cartItem.Quantity;

                cartItem.UpdateDetails(_mapper.Map<Product, ProductModel>(product));
            });
            
            _unitOfWork.Save();

            mappedOrder.Products = cart.Items;
            mappedOrder.Total = newOrder.Total;

            return mappedOrder;
        }

        public void CompleteOrder(OrderModel order)
        {
            throw new NotImplementedException();
        }

        public void CompleteOrder(int ID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OrderModel> GetAllOrders()
        {
            throw new NotImplementedException();
        }

        public OrderModel GetOrderByID(int id)
        {
            var order = _unitOfWork.Orders.GetByID(id);
            var orderItems = _unitOfWork.OrderItems.Get(item => item.OrderId == order.ID);

            var mappedOrder = _mapper.Map<Order, OrderModel>(order);
            mappedOrder.Products = orderItems.Select(item => new CartItem(_mapper.Map<ProductModel>(item.Product), item.Quantity, item.CurrentPrice)).ToList(); 
            
            return mappedOrder;
        }

        public PagedList<Order> GetPagedOrder(int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public void InvalidateOrder(OrderModel order)
        {
            throw new NotImplementedException();
        }

        public void InvalidateOrder(int ID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OrderModel> GetAllUserOrders(string userId)
        {
            var orders = _unitOfWork.Orders.Get(order => order.UserId.Equals(userId));
            var mappedOrders = _mapper.Map<IEnumerable<OrderModel>>(orders);

            return mappedOrders;
        }
    }
}
