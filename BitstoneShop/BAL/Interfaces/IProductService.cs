﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Utilities;
using Common.ViewModels;

namespace BAL.Interfaces
{
    public interface IProductService
    {
        bool AddNewProduct(ProductModel product);
        IEnumerable<ProductModel> GetAllProducts();
        IEnumerable<ProductModel> GetProductsByIds(IEnumerable<Int32> productIds);
        CartModel GetItemsFromCart(CartModel cart);
        ProductModel GetProductById(int id);
        bool EditProduct(ProductModel product);
        PagedList<ProductModel> GetPagedProducts(int pageNumber, int pageSize);

        bool DeleteProduct(int id);
    }
}
