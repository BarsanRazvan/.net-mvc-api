﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using Common.Utilities;
using Common.ViewModels;

namespace BAL.Interfaces
{
    public interface IOrderService
    {
        OrderModel GetOrderByID(int id);
        IEnumerable<OrderModel> GetAllOrders();
        PagedList<Order> GetPagedOrder(int pageSize, int pageNumber);
        OrderModel AddNewOrder(CartModel cart, string userId);
        void InvalidateOrder(OrderModel order);
        void InvalidateOrder(int ID);
        void CompleteOrder(OrderModel order);
        void CompleteOrder(int ID);

        IEnumerable<OrderModel> GetAllUserOrders(string userId);
    }
}
