﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BAL.Interfaces;
using BitstoneShop.MVC.API.V01.Models;
using BitstoneShop.MVC.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.IdentityModel.Tokens;
using Claim = System.Security.Claims.Claim;

namespace BitstoneShop.MVC.API.V01.Controllers
{
    [System.Web.Http.RoutePrefix("api/v01/token")]


    public class AccountController : ApiController
    {
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationSignInManager _signInManager;

        public AccountController()
        {
            this._userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            this._signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Token([FromBody] APILoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                var validPassword = await _userManager.CheckPasswordAsync(user, model.Password);

                if (validPassword)
                {
                    var secretKey = ConfigurationManager.AppSettings.Get("SecretKey");

                    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
                    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512);

                    var permClaims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim("valid", "1"),
                        new Claim("userid", user.Id),
                        new Claim(  "preferred_username", user.UserName),
                    };

                    permClaims.AddRange(_userManager.GetRoles(user.Id).Select(role => new Claim(System.Security.Claims.ClaimTypes.Role, role)));

                    var token = new JwtSecurityToken(
                        claims: permClaims,
                        expires: DateTime.Now.AddDays(1),
                        signingCredentials: credentials);

                    return Ok(new {
                        status = "success",
                        roles = _userManager.GetRoles(user.Id),
                        token = new JwtSecurityTokenHandler().WriteToken(token)
                    });
                }
            }

            return Content(HttpStatusCode.Unauthorized, new
            {
                status = "Failure",
                message = "Invalid credentials"
            });
        }


        [Authorize(Roles="Admin")]

        [HttpGet]
        [Route("")]
        public IHttpActionResult Token()
        {
            return Content(HttpStatusCode.Accepted, new
            {
                status = "success",
                message = "admin verified"
            });
        }

    }
}
