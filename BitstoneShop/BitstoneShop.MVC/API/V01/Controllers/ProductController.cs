﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BAL.Interfaces;
using Common.Utilities;
using Common.ViewModels;

namespace BitstoneShop.MVC.API.V01.Controllers
{
    [RoutePrefix("api/v01/products")]
    public class ProductController : ApiController
    {
        private IProductService _productService;

        public ProductController(IProductService productService)
        {
            this._productService = productService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(int pageNo, int pageSize)
        {
            if (pageSize < 0)
            {
                return Content(HttpStatusCode.BadRequest, new
                {
                    status = "failure",
                    message = "Invalid page size"
                });
            }

            if (pageNo < 1 )
            {
                return Content(HttpStatusCode.BadRequest, new
                {
                    status = "failure",
                    message = "Page number must be > 0"
                });
            }

            PagedList<ProductModel> products = _productService.GetPagedProducts(pageNo, pageSize);

            if (pageNo > products.TotalPages)
            {
                return Content(HttpStatusCode.BadRequest, new
                {
                    status = "failure",
                    message = "Page number exceeds number of pages",
                    totalPages = products.TotalPages
                });
            }

            return Content(HttpStatusCode.Accepted, new
            {
                status = "success",
                totalPages = products.TotalPages,
                numberOfProducts=  products.TotalCount,
                currentPage = products.CurrentPage,
                hasNextPage = products.HasNext,
                hasPreviousPage = products.HasPrevious,
                products = products
            });
        }

        [Route("{id}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var deletionSuccessful = _productService.DeleteProduct(id);

            if(deletionSuccessful)
            {
                return Ok(new
                {
                    status = "success",
                    message = "The product has been deleted successfully"
                });
            }

            return Content(HttpStatusCode.BadRequest, new
            {
                status = "failure",
                message = "make sure that the selected id is valid"
            });
        }

    }
}
