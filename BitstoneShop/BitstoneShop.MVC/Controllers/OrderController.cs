﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.Interfaces;
using Common.ViewModels;
using Microsoft.AspNet.Identity;

namespace BitstoneShop.MVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            this._orderService = orderService;
        }

        public ActionResult MyOrders()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            var orders = _orderService.GetAllUserOrders(userId);
            return View("UserOrders", orders);
        }


        [Route("{id}")]
        public ActionResult OrderDetails(int id)
        {
            var order = _orderService.GetOrderByID(id);
            return View("OrderDetails", order);
        }
    }
}