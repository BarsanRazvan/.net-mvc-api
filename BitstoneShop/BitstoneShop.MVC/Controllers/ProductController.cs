﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.Interfaces;
using Common.Entities;
using Common.Utilities;
using Common.ViewModels;

namespace BitstoneShop.MVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public ActionResult Index(int pageNo = 1, int pageSize = 10)
        {
            if (pageNo < 1 && pageSize < 1)
            {
                return View("ProductList", _productService.GetPagedProducts(1, 10));
            }

            return View("ProductList", _productService.GetPagedProducts(pageNo, pageSize));
        }

        [Authorize(Roles="Admin")]
        [HttpGet]
        public ActionResult Create()
        {
            return View("AddNewProduct");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(ProductModel product)
        {
            if (!ModelState.IsValid)
            {
                return View("AddNewProduct", product);
            }

            var addSucceeded = _productService.AddNewProduct(product);

            if (addSucceeded)
            {
                return RedirectToAction("Index", "Product");
            }

            return View("Error");

        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var product = _productService.GetProductById(id);

            if (product == null)
            {
                return RedirectToAction("Index", "Product");
            }

            return View("ProductEdit", product);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(ProductModel product)
        {
            if (!ModelState.IsValid)
            {
                return View("AddNewProduct", product);
            }

            _productService.EditProduct(product);

            return RedirectToAction("Index", "Product");
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var product = _productService.GetProductById(id);

            if (product == null)
            {
                return RedirectToAction("Index", "Product");
            }

            return View("ProductDetails", product);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            _productService.DeleteProduct(id);
            return RedirectToAction("Index", "Product");
        }
    }
}