﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BitstoneShop.MVC.Identity;
using Common.Entities;
using Common.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace BitstoneShop.MVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationSignInManager _signInManager;

        public AccountController()
        {
            this._userManager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            this._signInManager = System.Web.HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
        }

        [HttpGet]
        public ActionResult Register()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View("RegisterUser");
        }

        [HttpPost]
        public async Task<ActionResult> Register(UserRegistrationModel model)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View("RegisterUser", model);
            }

            var user = new User()
            {
                UserName = model.UserName,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber
            };

            var userCreated = await _userManager.CreateAsync(user, model.Password);

            if (userCreated.Succeeded)
            {
                var currentUser = _userManager.FindByName(user.UserName);

                await _userManager.AddToRoleAsync(currentUser.Id, "User");

                await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Error");
            }

        }

        [HttpGet]
        public ActionResult Login()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View("LoginPage");
        }

        [HttpPost]
        public async Task<ActionResult> Login(UserLoginModel user)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View("LoginPage", user);
            }

            var signedIn = await _signInManager.PasswordSignInAsync(user.Username, user.Password, isPersistent: false, shouldLockout: false);
            if (signedIn == SignInStatus.Success)
            {
                return RedirectToAction("Index", "Home");
            }


            return View("LoginPage", user);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            _signInManager.AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}