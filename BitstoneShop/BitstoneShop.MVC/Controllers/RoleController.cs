﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BitstoneShop.MVC.Identity;
using Common.Entities;
using Common.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace BitstoneShop.MVC.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        private ApplicationRoleManager _roleManager;

        public RoleController()
        {
            _roleManager = System.Web.HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View("Role");
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoleModel roleModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }

            var role = new Role { Name = roleModel.Name };

            var result = await this._roleManager.CreateAsync(role);

            if (!result.Succeeded)
            {
                return View("Error");
            }


            return RedirectToAction("Index", "Home");
        }
    }
}