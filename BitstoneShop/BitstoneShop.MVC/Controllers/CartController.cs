﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BAL.Interfaces;
using BitstoneShop.MVC.Identity;
using Common.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace BitstoneShop.MVC.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly ApplicationUserManager _userManager;

        public CartController(IOrderService orderService, IProductService productService)
        {
            _orderService = orderService;
            _productService = productService;
            _userManager = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }

        private CartModel _getSessionProducts()
        {
            if (this.Session["UserCart"] != null)
            {
                return this.Session["UserCart"] as CartModel;
            }
            else
            {
               return new CartModel();
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            var cart = this._getSessionProducts();
            var products = _productService.GetItemsFromCart(cart);
            return View("CartList", products);
        }

        [HttpPost]
        public ActionResult AddItemToCart(int id)
        {
            if (_productService.GetProductById(id) == null)
            {
                return Json(new { status = "failed" });
            }

            var cart = this._getSessionProducts();
            cart.AddItem(id, 1);
            this.Session["UserCart"] = cart;
            return Json(new {status= "success" });
        }

        [HttpPost]
        public ActionResult ChangeQuantity(int productId, int quantity)
        {
            var cart = _getSessionProducts();
            cart.ChangeProductQuantity(productId, quantity);
            this.Session["UserCart"] = cart;
            return Json(new {status = "success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFromCart(int productId)
        {
            var cart = _getSessionProducts();
            cart.DeleteProduct(productId);
            this.Session["UserCart"] = cart;
            return Json(new { status = "success" });
        }

        [HttpPost]
        public ActionResult ClearCart()
        {
            this.Session["UserCart"] = new CartModel();
            return Json(new { status = "success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitOrder()
        {
            var cart = _getSessionProducts();

            var userId = HttpContext.User.Identity.GetUserId();
            var order = _orderService.AddNewOrder(cart, userId);
            this.Session["UserCart"] = new CartModel();

            var user = _userManager.FindById(userId);
            order.User = user;

            return View("~/Views/Order/OrderDetails.cshtml", order);
        }
    }
}