using System;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using AutoMapper;
using BAL.Interfaces;
using BAL.Services;
using Common.Profiles;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using Unity;


namespace BitstoneShop.MVC
{
    public static class UnityConfig
    {
        public static IUnityContainer Container;
        public static void RegisterComponents()
        {
            Container = new UnityContainer();

            // e.g. container.RegisterType<ITestService, TestService>();
            Container.RegisterType<IUnitOfWork, UnitOfWork>();
            Container.RegisterType<IProductService, ProductService>();
            Container.RegisterType<IOrderService, OrderService>();

            var profiles = typeof(OrderProfile).Assembly.GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));

            MapperConfiguration configuration = new MapperConfiguration(cfg => {
                
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(Activator.CreateInstance(profile) as Profile);
                }
            });

            Container.RegisterInstance<IMapper>(configuration.CreateMapper());


            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(Container));
        }
    }
}