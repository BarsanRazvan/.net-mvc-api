﻿function addToCart(id)
{
    $.ajax({
        method: "POST",
        url: "../cart/AddItemToCart",
        data: {
            id: id
        },
        success: (data) => { console.log(data); }
    });
}


function updateTotal(id, increment, quantity = null) {
    $total = $('#total-tag');
    $price = $(".price[data-id='" + id + "']");
    var newTotal = $total.text() * 1;

    if (quantity != null) {
        increment = increment * quantity;
    }

    newTotal = newTotal + increment * $price.text();
    $total.text(newTotal);
}


function increaseQuantity(id) {
    var $item = $(".quantity[data-id='" + id + "']");
    var itemQuantity = $item.data("quantity") * 1;
    $item.data("quantity", itemQuantity + 1);
    $item.text(itemQuantity + 1);
    console.log(id, typeof id);
    console.log(itemQuantity, typeof itemQuantity);
    updateTotal(id, 1);
    $.ajax({
        method: "POST",
        url: "../cart/ChangeQuantity",
        data: {
            productId: id,
            quantity: itemQuantity + 1
        },
        success: (data) => { console.log(data); }
    });
}

function decreaseQuantity(id) {
    var $item = $(".quantity[data-id='" + id + "']");
    var itemQuantity = $item.data("quantity") * 1;
    console.log(id, typeof id);
    console.log(itemQuantity, typeof itemQuantity);

    if (itemQuantity < 2) {
        deleteFromCart(id);
    } else {
        $item.data("quantity", itemQuantity - 1);
        $item.text(itemQuantity - 1);
    }

    updateTotal(id, -1);

    $.ajax({
        method: "POST",
        url: "../cart/ChangeQuantity",
        data: {
            productId: id,
            quantity: itemQuantity - 1
        },
        success: (data) => { console.log(data); }
    });
}

function deleteFromCart(id) {
    var $item = $(".product-row[data-id='" + id + "']");
    var itemQuantity = $(".quantity[data-id='" + id + "']").data("quantity") * 1;
    updateTotal(id, -1, itemQuantity);
    $item.remove();
    $.ajax({
        method: "POST",
        url: "cart/DeleteFromCart",
        data: {
            productId: id
        },
        success: (data) => { console.log(data); }
    });
}

function clearCart() {
    var $item = $(".product-row");
    $.each($item, (key, $value) => {
        $value.remove();
    })

    $.ajax({
        method: "POST",
        url: "cart/ClearCart",
        success: (data) => { console.log(data); }
    });
}


function placeOrder(id) {
    $.ajax({
        method: "POST",
        url: "cart/PlaceOrder",
        data: {
            id: id
        },
        success: (data) => { console.log(data); }
    });
}

(function ($) {
})(jQuery);