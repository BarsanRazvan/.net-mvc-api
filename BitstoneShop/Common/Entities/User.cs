﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Common.Entities
{
    public class User : IdentityUser
    {
        public virtual ICollection<Order> Order { get; set; }

    }
}
