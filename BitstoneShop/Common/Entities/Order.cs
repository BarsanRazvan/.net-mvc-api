﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Order
    {
        public int ID { get; set; }
        public DateTime OrderDate { get; set; }
        public float Total { get; set; }
        public string UserId { get; set; }

        public virtual ICollection<OrderItem> OrderItem { get; set; }
        public virtual User User { get; set; }
    }
}
