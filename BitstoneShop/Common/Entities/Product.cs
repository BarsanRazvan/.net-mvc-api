﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public float Price { get; set; }
        public ProductCategory Category { get; set; }
        public string Description { get; set; }

        public virtual ICollection<OrderItem> OrderItem { get; set; }
    }
}
