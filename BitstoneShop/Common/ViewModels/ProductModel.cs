﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ViewModels
{
    public class ProductModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Please enter name")] 
        [MaxLength(30)]
        public string Name { get; set; }


        [Required(ErrorMessage = "Please enter price")]
        public float Price { get; set; }

        [Required]
        public ProductCategory Category { get; set; }

        [Required(ErrorMessage = "Please enter a description")]
        [MaxLength(100)]
        public string Description { get; set; }
    }
}
