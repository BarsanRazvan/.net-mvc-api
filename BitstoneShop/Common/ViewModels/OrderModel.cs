﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;

namespace Common.ViewModels
{
    public class OrderModel
    {
        public int ID { get; set; }
        public DateTime OrderDate { get; set; }
        public float Total { get; set; }
        public User User { get; set; }
        public List<CartItem> Products { get; set; }
    }
}
