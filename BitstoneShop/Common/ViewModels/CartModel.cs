﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;

namespace Common.ViewModels
{
    [Serializable]
    public class CartModel
    {
        public List<CartItem> Items { get; set; }

        public CartModel()
        {
            Items = new List<CartItem>();
        }

        public void AddItem(int id, int quantity)
        {
            if (Items.Any(item => item.ID == id))
            {
                return;
            }

            Items.Add(new CartItem(id, quantity));
        }

        public float GetTotal()
        {
            return Items.Sum(item => item.Price * item.Quantity);
        }

        public void ChangeProductQuantity(int id, int quantity)
        {
            Items = Items.Select(item =>
                {
                    if(item.ID == id)
                        item.Quantity = quantity; 
                    return item;
                })
                         .ToList();
        }

        public void DeleteProduct(int id)
        {
            Items = Items.Where(item => item.ID != id).ToList();
        }

        public void UpdateProductDetails(List<ProductModel> products)
        {
            Items = Items.Select(item => {
                                var modeledProduct = products.FirstOrDefault(product => product.ID == item.ID);
                                item.UpdateDetails(modeledProduct);
                                return item; })
                        .ToList();
        }

    }
}
