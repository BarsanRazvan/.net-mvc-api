﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ViewModels
{
    public class CartItem : ProductModel
    {
        public int Quantity { get; set; }

        public CartItem()
        {

        }

        public CartItem(int id, int quantity)
        {
            this.ID = id;
            this.Quantity = quantity;
        }

        public CartItem(ProductModel product, int quantity, float price)
        {
            this.Category = product.Category;
            this.Price = price;
            this.Description = product.Description;
            this.ID = product.ID;
            this.Name = product.Name;
            this.Quantity = quantity;
        }

        public void UpdateDetails(ProductModel product)
        {
            this.Price = product.Price;
            this.Category = product.Category;
            this.Description = product.Description;
            this.Name = product.Name;
        }
    }
}
