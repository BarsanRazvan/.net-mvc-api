﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ViewModels
{
    public class UserRegistrationModel
    {
        [Required(ErrorMessage = "Please insert a username")]
        [MinLength(3, ErrorMessage = "Username must be at least 3 characters long")]
        [MaxLength(15, ErrorMessage = "Username must have a maximum of 10 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please insert an email")]
        [EmailAddress(ErrorMessage = "Email address is not valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please insert a password")]
        [DataType(DataType.Password)]
        [Compare("ConfirmPassword", ErrorMessage = "Passwords must match")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please insert a password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
